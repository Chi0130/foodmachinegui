import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodMachineGUI {
    private JPanel root;
    private JLabel Topllabel;
    private JLabel totalinfo;
    private JLabel orderditem;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton curryRiseButton;
    private JButton tenshinhanButton;
    private JButton sobaButton;
    private JButton checkOutButton;
    private JTextPane info1;
    int Total =0;


    void order(String food){
        int comfirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (comfirmation==0) {

            JOptionPane.showMessageDialog(null,"Thank you. Order for " + food + " received.");
            Total+=100;
            info1.setText(info1.getText()+food+"\n");
            totalinfo.setText("Total "+Total+"yen");

        }
}

    public FoodMachineGUI() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        curryRiseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("CurryRise");
            }
        });
        tenshinhanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tenshinhan");
            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int comfirmation2 = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?",
                        "Check out Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (comfirmation2==0) {

                    JOptionPane.showMessageDialog(null,"Thank you. Total price is " + Total + " yen.");
                    Total=0;
                    totalinfo.setText("Total "+Total+"yen");
                    info1.setText("");
            }
        }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodMachineGUI");
        frame.setContentPane(new FoodMachineGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
